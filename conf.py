import os

exclude_patterns = ["README.rst"]

extensions = [
    "sphinx_reredirects"
]

lang = os.environ["SCHEME_TUTORIAL_RTD_BUILD_LANGUAGE"]
redirects = {
    docname: f"https://extending-lilypond.readthedocs.io/{lang}/latest/scheme/{new_name}.html"
    for docname, new_name in [
            ("index", "index"),
            ("demarrer", "first-steps"),
            ("expressions", "expressions"),
            ("fonctions", "functions"),
            ("messages", "messages"),
            ("conditions", "conditions"),
            ("variables-locales", "local-variables"),
            ("listes", "lists"),
            ("quoting", "quoting"),
            ("alists", "alists"),
            ("recursivite", "recursion"),
            ("continuer", "after"),
    ]
}
