This repository used to host a Scheme tutorial. It has now been merged
into https://extending-lilypond.readthedocs.io. The new repository is
https://gitlab.com/jeanas/extending-lilypond. This repository now just
contains redirects from the old pages to the new ones at
https://extending-lilypond.readthedocs.io.
